const Emails = new Mongo.Collection("emails");

if (Meteor.isServer) {
  Emails.allow({
    insert() {
      return true;
    },
    update(userId, doc, fields, modifier) {
      return true;
    },
    remove(userId, doc, fields, modifier) {
      return false;
    }
  });
}

export default Emails;
