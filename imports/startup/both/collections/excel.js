const Excel = new Mongo.Collection("excel");

if (Meteor.isServer) {
  Excel.allow({
    insert() {
      return true;
    },
    update(userId, doc, fields, modifier) {
      return true;
    },
    remove(userId, doc, fields, modifier) {
      return false;
    }
  });
}

export default Excel;
