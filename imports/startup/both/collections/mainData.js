
const MainData = new Mongo.Collection("mainData");

if(Meteor.isServer){
  MainData._ensureIndex({
    projectDescription: "text",
    contactDetails: "text",
    organizationDetails: "text",
    projectRelation: "text",
    projectFocus: "text"
  });

  MainData._ensureIndex({
    projectDescription: 1
  })




  MainData.allow({
    insert() {
      return true;
    },
    update(userId, doc, fields, modifier) {
      return true;
    },
    remove(userId, doc, fields, modifier) {
      return false;
    }
  });
}
 


export default MainData;