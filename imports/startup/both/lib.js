export const ROBOT = "🤖 ";
export const SCHEDULE_MINUTE = 0;
// SELECTORS

export const totalProjects =
  "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > h1 > span";

export const projectLink =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div.faceted-search-result-list > div.tool-search-result > ul > li > div > h2 > a";
// 1
export const projectTitle =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.tool-item-data.tool-data.main-col > h1";
// 2
export const projectSummary =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.tool-item-data.tool-data.main-col > p";
// 3
export const projectDescription =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.tool-item-data.tool-data.main-col > div.project-description.description.running-text";
//4
export const partnerCount =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.shadow > div:nth-child(1) > div > div.h3.mrgn-btm-3";
// 5
export const partnersFrom =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.shadow > div:nth-child(1) > div > div.microcopy";
// 6
export const projectDeadline =
  "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.shadow > div:nth-child(1) > div > div.h3.mrgn-btm-6";
// 7-16
export const contactDetails =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.shadow > div.otlas-contact.mrgn-btm-33 > div";
// 17
export const organizationName =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.mrgn-btm-11 > a";
// 18-19
export const projectDates =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.project-meta.mrgn-btm-33";
// 20
export const projectRelation =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.project-action";
// 21
export const projectFocus =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.tags > ul";
// 22
export const projectIncludes =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.project-inclusioninfo.microcopy.mrgn-btm-33 > ul";
// 23
export const projectIncludesDescription =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.project-inclusioninfo.microcopy.mrgn-btm-33 > div";
// 24
export const projectShortLink =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div:nth-child(8) > p.short-url.mrgn-btm-22";
// 25-26
export const organizationDescription =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.tool-item-data.organisation-data.main-col > div.organisation-description.running-text";

export const organizationLink =
         "body > div.page.clearfix > div.site-body.wrapped.salto-layout-slot\\:template-scoping-class.shadow > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.project-overview > div.mrgn-btm-11 > a";

export const projectClosed =
  "body > div.page.clearfix > div.site-body.wrapped.layout.shadow.otlas > div.tile.tile-wide > div.tool-content.clearfix.fx-content.-content > div > div > div.aside > div.shadow > div:nth-child(1) > div > div.status-found";
