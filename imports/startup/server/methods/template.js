import * as CHALK from "../chalk.js";

let activelySearchingPartners = Assets.getText(
  "actively-searching-partners.html"
);
let passivelySearchingPartners = Assets.getText(
  "passively-searching-partners.html"
);
let notSearchingPartners = Assets.getText("not-searching-partners.html");

SSR.compileTemplate("activelySearchingPartners", activelySearchingPartners);
SSR.compileTemplate("passivelySearchingPartners", passivelySearchingPartners);
SSR.compileTemplate("notSearchingPartners", notSearchingPartners);

Meteor.methods({
  renderTemplate(projectName, contactName, shortLink, partnerStatus) {
    console.log("renderTemplateCalled:", projectName, contactName, shortLink, partnerStatus)
    check(projectName, String);
    check(contactName, String);
    check(shortLink, String);
    check(shortLink, String);

    switch (partnerStatus) {
      case "active":
        console.log(
          CHALK.infoMsg(
            "Sending mail to organizations actively searching partners."
          )
        );
        return SSR.render("activelySearchingPartners", {
          projectName: projectName,
          contactName: contactName,
          shortLink: shortLink
        });
      case "passive":
        console.log(
          CHALK.infoMsg(
            "Sending mail to organizations passively searching partners."
          )
        );
        return SSR.render("passivelySearchingPartners", {
          projectName: projectName,
          contactName: contactName,
          shortLink: shortLink
        });

      case "notSearching":
        console.log(
          CHALK.infoMsg("Sending mail to organizations not searching partners.")
        );
        return SSR.render("notSearchingPartners", {
          projectName: projectName,
          contactName: contactName,
          shortLink: shortLink
        });

      default:
          return SSR.render("activelySearchingPartners", {
            projectName: projectName,
            contactName: contactName,
            shortLink: shortLink,
          });
    }
  }
});
