import MainData from "../../both/collections/mainData.js";
import Excel from "../../both/collections/excel.js";
import * as CHALK from "../chalk.js";
const json2xls = require("json2xls");
const fs = require("fs");
const schedule = require("node-schedule");

const automateExcel = new schedule.RecurrenceRule();
automateExcel.dayOfWeek = [new schedule.Range(0, 6)];
automateExcel.minute = Meteor.settings.crawler.automatedExcelSchedule;

const excelSchedule = schedule.scheduleJob(
  automateExcel,
  async function convertToExcel() {
    console.log("Getting excel...");
    let path = process.cwd();
    let json = MainData.find({ projectTitle: { $exists: true } }).fetch();
    console.log(
      CHALK.success("Fetched "),
      json.length,
      CHALK.success("documents.")
    );
    let jsonArray = [];

    json.forEach(project => {
      let contactPerson = () => {
        let contact = project.contactDetails.filter(item =>
          item.startsWith("Contact")
        )[0];
        return contact ? contact.split(": ")[1] : "No Contact Person";
      };

      let primaryEmail = () => {
        let email = project.contactDetails.filter(item =>
          item.startsWith("E-mail")
        )[0];

        return email ? email.split(": ")[1] : "No E-mail";
      };

      let secondaryEmail = () => {
        let email = project.contactDetails.filter(item =>
          item.startsWith("E-mail")
        )[0];

        return email ? email.split(": ")[1] : "No E-mail";
      };

      let tertiaryEmail = () => {
        let email = project.contactDetails.filter(item =>
          item.startsWith("E-mail")
        )[0];

        return email ? email.split(": ")[1] : "No E-mail";
      };

      let phoneNumber = () => {
        let phone = project.contactDetails.filter(item =>
          item.startsWith("Phone")
        )[0];

        return phone ? phone.split(": ")[1] : "No Phone Number";
      };
      let mobileNumber = () => {
        let mobile = project.contactDetails.filter(item =>
          item.startsWith("Mobile")
        )[0];

        return mobile ? mobile.split(": ")[1] : "No Mobile Number";
      };

      let faxNumber = () => {
        let fax = project.contactDetails.filter(item =>
          item.startsWith("Fax")
        )[0];

        return fax ? fax.split(": ")[1] : "No Fax Number";
      };

      let facebook = () => {
        let fb = project.contactDetails.filter(item =>
          item.startsWith("Facebook")
        )[0];

        return fb ? fb.split(": ")[1] : "No Facebook";
      };

      let skypeLink = () => {
        let skype = project.contactDetails.filter(item =>
          item.startsWith("Skype")
        )[0];

        return skype ? skype.split(": ")[1] : "No Skype";
      };
      let twitter = () => {
        let tw = project.contactDetails.filter(item =>
          item.startsWith("Skype")
        )[0];

        return tw ? tw.split(": ")[1] : "No Twitter";
      };
      try {
        let json = {
          "Project Title": project.projectTitle,
          "Short Description": project.projectSummary,
          Description: project.projectDescription,
          "Partners Needed": project.partnerCount,
          "Partners From": project.partnersFrom,
          Deadline: project.projectDeadline,
          "Contact Person": contactPerson(),
          "Primary E-mail": primaryEmail(),
          "Seconday E-Mail": secondaryEmail(),
          "Tertiary E-Mail": tertiaryEmail(),
          "Phone Number": phoneNumber(),
          Fax: faxNumber(),
          Mobile: mobileNumber(),
          Facebook: facebook(),
          Skype: skypeLink(),
          Twitter: twitter(),
          "Organization Name": project.organizationName,
          "Start Date": project.projectStartDate,
          "End Date": project.projectEndDate,
          "Project Relation": project.projectRelation,
          "Project Focus": project.projectFocus,
          "Project Includes": project.projectIncludes,
          "Short URL": project.projectShortLink,
          "Organization Country": project.organizationDetails.base,
          "Organization Type": project.organizationDetails.type,
          "Organization Website": project.organizationLink
        };
        jsonArray.push(json);
      } catch (error) {
        console.error(
          "Project details faulty:",
          project.projectTitle,
          project.projectShortLink
        );
      }
    });

    let xls = json2xls(jsonArray);
    console.log(CHALK.infoMsg("Writing json to excel file."));
    fs.writeFileSync(path + "/projects.xlsx", xls, "binary");
    console.log(CHALK.infoMsg("Reading from file."));
    let excelBuffer = fs.readFileSync(path + "/projects.xlsx");

    console.log(CHALK.warnMsg("Inserting buffer to database"));
    Excel.upsert(
      {
        filename: "projects.xlsx"
      },
      {
        filename: "projects.xlsx",
        buffer: excelBuffer,
        lastModified: new Date().toUTCString()
      }
    );
    console.log(CHALK.success("Excel file updated!"));
    return excelBuffer;
  }
);

Meteor.methods({
  async getExcelBuffer() {
    let doc = await Excel.findOne({ filename: "projects.xlsx" });

    console.dir(doc);
    return {
      fileName: doc.fileName,
      buffer: doc.buffer,
      lastModified: doc.lastModified
    };
  }
});
