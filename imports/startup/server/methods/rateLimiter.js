import { DDPRateLimiter } from "meteor/ddp-rate-limiter";

const searchRule = {
  type: "method",
  name: "search"
}

DDPRateLimiter.addRule(searchRule, 10, 1000, ()=> console.log(DDPRateLimiter.getErrorMessage()));

DDPRateLimiter.setErrorMessage(()=> (`You are making too frequent requests. Please wait for 200 ms before making new request.`))
