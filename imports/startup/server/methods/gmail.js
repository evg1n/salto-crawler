import gmailController from "../../../controllers/gmailController.js";
import emailTemplate from "./template.js";
import Emails from "../../both/collections/emails.js";
import MainData from "../../both/collections/mainData.js";
import { gmail } from "googleapis/build/src/apis/gmail";
import * as CHALK from "../chalk.js";

const CREDENTIALS = Meteor.settings.google_api_credentials;
const GMAIL_CLIENT = gmailController.authorize(CREDENTIALS);

Meteor.methods({
  listLabels() {
    gmailController.listLabels(GMAIL_CLIENT);
  },

  async getInbox() {
    console.log(CHALK.infoMsg("Retrieving messages..."));
    let inbox = await gmailController.getInboxList(GMAIL_CLIENT);
    return await gmailController.getMessages(GMAIL_CLIENT, inbox);
  },

  async sendMail(emailObject) {
    check(emailObject, Object);
    console.log(CHALK.infoMsg("Sending message"));
    return await gmailController.mailSend(GMAIL_CLIENT, emailObject);
  },

  async sendBulkMail(contactDetailsArray) {
    console.log(CHALK.infoMsg("Mailer started."), contactDetailsArray);

    check(contactDetailsArray, Array);

    for (let contactDetails of contactDetailsArray) {
      let {
        contactPerson,
        emails,
        subject,
        shortLink,
        organizationBase,
        projectId,
      } = contactDetails;

      let emailObject = {
        from: "kiyidosk@gmail.com",
        to: emails.join(", "),
        subject: subject,
        link: shortLink,
        base: organizationBase,
        projectId: projectId,
        contactPerson: contactPerson,
      };

      let project = MainData.findOne(
        { _id: projectId },
        { sort: { publishedAt: -1 } }
      );

      const getType = (project) => {
        let { partnerCount, projectEndDate } = project;

        let today = new Date();
        let projectExpired = projectEndDate <= today ? true : false;
        let projectActive = projectEndDate >= today ? true : false;
        let projectSearching = partnerCount > 0;

        console.log("projectExpired:", projectExpired);
        console.log("projectActive:", projectActive);
        console.log("projectSearching:", projectSearching, partnerCount);
        if (projectExpired && projectSearching) {
          console.log("Passive");
          
          emailObject.type = "passive";
        }

        if (projectActive && projectSearching) {
          console.log("Active");

          emailObject.type = "active";
        }

        if (projectActive && !projectSearching) {
          console.log("Not searching");
          emailObject.type = "notSearching";
        }

        if (projectExpired) {
          console.log("Not searching");
          emailObject.type = "expired";
        }

        let message = Meteor.call(
          "renderTemplate",
          subject,
          contactPerson,
          shortLink,
          emailObject.type
        );
        emailObject.message = message;
      };

      await getType(project);

      if (!emailObject.base.startsWith("Turkey")) {
        let response = await gmailController.mailSend(
          GMAIL_CLIENT,
          emailObject
        );

        console.log(
          CHALK.warnMsg("Sending mail to email addresses:"),
          emailObject.to
        );
        console.dir(emailObject);
        Emails.update(
          { projectId: projectId },
          {
            $set: {
              threadId: response.data.threadId,
              mailSent: new Date(),
            },
          }
        );
      } else {
        console.log(
          CHALK.warnMsg("Skipping project:"),
          emailObject.subject,
          CHALK.infoMsg(". Because it is based in"),
          "TURKEY"
        );
        Emails.update(
          { projectId: projectId },
          {
            $set: {
              threadId: "noThread",
              mailSent: new Date(),
            },
          }
        );

        continue;
      }
    }
    console.log(CHALK.success("Mailer finished."));
    return "Success.";
  },

  async getThread(shortLink) {
    console.log("ShortLink:", shortLink)
    let threadsArray = [];
    let emailThreads = [];
    let emailObject = Emails.findOne({ shortLink: shortLink });
    console.log(emailObject);
    threadsArray.push(emailObject.threadId);

    for (let thread of threadsArray) {
      let emailThread = await gmailController.getThread(GMAIL_CLIENT, thread);
      console.log("emailThread", emailThread.data);
      emailThreads.push(emailThread.data);
    }
    return emailThreads;
  },
});
