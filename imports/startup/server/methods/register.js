import { Accounts } from "meteor/accounts-base"

Meteor.methods({

  register(options){
    console.log("createuser")

    let {username, email, password, profile} = options;

    Accounts.createUser({username: username, password: password, email: email, profile: profile});

  },

  logger(input){
    console.log("log:", input);
  }




})

if (Meteor.isServer){
  Accounts.onCreateUser((options, user) => {
    
    Accounts.sendVerificationEmail(user._id)
    return user
  })

}