import MainData from "../../both/collections/mainData.js";
import * as CHALK from "../chalk.js";

const queryString = (skip, limit) =>
  `https://www.salto-youth.net/tools/otlas-partner-finding/projects/?b_order=lastmod&b_offset=${skip}&b_limit=${limit}&b_name=&b_range_projects_begin_date_day=27&b_range_projects_begin_date_month=3&b_range_projects_begin_date_year=2010&b_range_projects_end_date_day=27&b_range_projects_end_date_month=3&b_range_projects_end_date_year=2025&format=rss&preventPersistentGetParameters=1`;

let Parser = require("rss-parser");
let parser = new Parser();

let rootURL = "https://www.salto-youth.net/tools/otlas-partner-finding/";

export const updateProjects = async () => {
  console.log(CHALK.infoMsg("Checking latest projects..."));
  let feed = await parser.parseURL(queryString(0, 100));

  for (let item of feed.items){

let shouldUpdate = await MainData.findOne({route: item.link.split(rootURL)[1]})

    if (!shouldUpdate) {
      console.log(CHALK.warnMsg("Project not found! Inserting now."));
      MainData.upsert(
        {
          route: item.link.split(rootURL)[1]
        },
        {
          $set: {
            publishedAt: new Date(item.pubDate),
            route: item.link.split(rootURL)[1],
            createdAt: new Date()
          }
        }
      );
    } else {
      MainData.update(
        {
          route: item.link.split(rootURL)[1], publishedAt: {$exists: false}
        },
        {
          $set: {
            publishedAt: new Date(item.pubDate),
          }
        }
      );
    }
  }
  console.log(CHALK.success("Updated projects."));
};
