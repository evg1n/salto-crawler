import searchController from "../../../controllers/searchController.js";
import { findDOMNode } from "react-dom";

Meteor.methods({
  async search(searchQuery, skip, limit) {
    check(searchQuery, Object);

    this.unblock();

    try {
      let results = await searchController.searchExactMatch(
        searchQuery,
        skip,
        limit
      );
      return results;
    } catch (error) {
      return {
        status: 500,
        message: "Search failed: " + JSON.stringify(error)
      };
    }
  },

  async getAllProjects(searchQuery, skip, limit) {
    console.log("getting all projects");
    try {
      return await searchController.getAllProjects(skip, limit);
    } catch (error) {
      return error;
    }
  },

  async findOne(projectId){
    try{
      return searchController.findOne(projectId);
    } catch(error){
      return error
    }
  }
});
