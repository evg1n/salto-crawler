import { Meteor } from "meteor/meteor";
import puppet from "../puppeteer/puppeteer.js";
import MainData from "../../both/collections/mainData.js";
import Emails from "../../both/collections/emails.js";

import * as CHALK from "../chalk.js";
import { updateProjects } from "./rssFeed.js";

Meteor.methods({
  async crawl() {
    const start = process.hrtime();
    const end = process.hrtime(start);
    console.log(CHALK.warnMsg(start));

    const baseURL = Meteor.settings.crawler.baseURL;
    const offset = Meteor.settings.crawler.offset;
    const limit = Meteor.settings.crawler.limit;

    let links = await puppet.crawl(baseURL, offset, limit);

    if (links.length && links.length > 0) {
      for (link of links) {
        try {
          let shouldUpdate = MainData.findOne({ route: link });
          if (!shouldUpdate) {
            console.log(CHALK.infoMsg("Inserting link"), link);
            MainData.upsert({ route: link }, { route: link });
          } else {
            console.log(CHALK.warnMsg("Skipping:"), link);
            continue;
          }
        } catch (error) {
          console.log(CHALK.errMsg("Error on link:"), link);
          console.error("Mongo insert error:", error);
          continue;
        }
      }
    }
    console.log(CHALK.success("Crawled %d links"), links.length);
    return {
      status: 200,
      body: {
        message: "Refreshed links",
        linkCount: links.length
      }
    };
  },

  async parseDOM() {
    console.log(CHALK.infoMsg("DOM Parser Launched"));
    let links = await MainData.find(
      {},
      { fields: { route: 1, projectTitle: 1 } }
    ).fetch();

    let baseURL = "https://www.salto-youth.net/tools/otlas-partner-finding/";

    for (link of links) {
      let url = baseURL + link.route;

      if (link.projectTitle) {
        console.log(CHALK.warnMsg("Skipping:"), link.route);
        continue;
      } else {
        console.log("Parsing link:", link.route);
        let detail = await puppet.getDetails(url);
        console.dir("Details", link);
        console.dir(detail);
        console.log(CHALK.errMsg(detail.projectStartDate))
        try {

          let detailObj = {...detail};

          detailObj.projectStartDate = new Date(detail.projectStartDate);
          detailObj.projectEndDate = new Date(detail.projectEndDate);
          detailObj.projectDeadline = new Date(detail.projectDeadline);
          detailObj.createdAt = new Date(detail.createdAt);

          MainData.upsert({ route: link.route }, { $set: { ...detailObj } });
        } catch (error) {
          console.error(error);
          continue;
        }
      }
    }

    console.log(CHALK.success("Parse complete!"));

    return {
      status: 200,
      body: {
        message: "Crawling succeeded!"
      }
    };
  },
  async getContactDetails() {
    console.log(CHALK.infoMsg("Getting contact details for projects."));
    let projects = await MainData.find(
      { projectTitle: { $exists: true } },
      {
        fields: {
          _id: 1,
          projectTitle: 1,
          contactDetails: 1,
          projectShortLink: 1,
          organizationDetails: 1,
          publishedAt: 1
        }, sort: {publishedAt: -1}
      }
    ).fetch()

    console.log("Projects to crawl:", projects.length);
    try {
      for (let project of projects) {
        try {
          let emailObject = {
            projectId: project._id,
            subject: project.projectTitle,
            contactPerson: undefined,
            emails: [],
            shortLink: project.projectShortLink,
            organizationBase: project.organizationDetails.base,
            createdAt: new Date(),
            publishedAt: project.publishedAt
          };

          try {
            if (project.contactDetails.length > 0) {
              let emails = [];
              let contactPerson = "Representative";
              project.contactDetails.forEach(item => {
                if (item.startsWith("E-mail:")) {
                  emails.push(item.split("E-mail: ")[1]);
                }

                if (item.startsWith("Contact person:")) {
                  contactPerson = item.split("Contact person: ")[1];
                }
              });
              emailObject.emails = emails;
              emailObject.contactPerson = contactPerson;
            }
            

            let shouldUpdate = Emails.findOne({ shortLink: project.projectShortLink, "emails.0": {$exists: true}});

            if (!shouldUpdate) {
              console.log("Updating contact details of:", project.projectTitle)
              await Emails.upsert(
                { projectId: project._id },
                { ...emailObject }
              );
              console.log(CHALK.infoMsg("Inserting document:"), emailObject);
            } else {
              console.log(CHALK.warnMsg("Skipping:", project.projectTitle));
            }
            continue;
          } catch (error) {
            console.error(
              CHALK.errMsg("Error on project:", project.projectTitle)
            );
            console.error("Details:", error);
            continue;
          }
        } catch (error) {
          console.log("faulty:", error);
          continue;
        }
      }
       return {
         status: 200,
         body: {
           message: "Contact details for mailing list has been updated!"
         }
       };
    } catch (error) {
      console.log(error);
    }
  },
  async updateAllProjects() {
    return updateProjects();
  },

  async sentEmails() {
    return Emails.find({ mailSent: { $exists: true } }).fetch();
  }
});