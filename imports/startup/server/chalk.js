import chalk from "chalk";

chalk.level = 3;


const infoMsg = chalk.rgb(100,213,205);
const errMsg = chalk.white.bgRed;
const warnMsg = chalk.rgb(237,193,59)
const success = chalk.rgb(111,233,82);

export {
  infoMsg,
  errMsg,
  warnMsg,
  success
}