import puppeteer from "puppeteer";
import MainData from "../../both/collections/mainData.js";

import * as SELECTORS from "../../../startup/both/lib.js";
import * as CHALK from "../chalk.js";
import Emails from "../../both/collections/emails.js";

const _query = (offset, limit) => {
  return `?b_order=lastmod&b_offset=${offset}&b_limit=${limit}&b_name=&b_range_projects_begin_date_day=28&b_range_projects_begin_date_month=2&b_range_projects_begin_date_year=2020&b_range_projects_end_date_day=28&b_range_projects_end_date_month=2&b_range_projects_end_date_year=2025`;
};

const _calculateIterations = (totalProjects, limit) => {
  let projectCount = parseInt(totalProjects);
  let divider = parseInt(limit);
  let iterations = Math.ceil(projectCount / divider);
  console.log(CHALK.infoMsg("Total pages:", iterations));
  return iterations;
};

const _puppeteerOptions = {
  defaultViewport: {
    width: 1200,
    height: 800
  },
  headless: true,
  args: ["--no-sandbox", "--disable-setuid-sandbox"]
};

class Puppeteer {
  constructor() {
    this.iterations = _calculateIterations;
  }
  crawl = async (baseURL, offset, limit) => {
    try {
      const browser = await puppeteer.launch(_puppeteerOptions);
      const page = await browser.newPage();

      await page.setJavaScriptEnabled(false);

      console.log(CHALK.infoMsg("New browser instance started."));

      let url = baseURL + _query(offset, limit);
      let projectLinks = [];

      await page.goto(url, { waitUntil: "domcontentloaded" });

      console.log(CHALK.infoMsg("DOM content loaded."));
      
        let pagination = await page.evaluate(
          (SELECTORS, limit) => {
            let totalProjects = document.getElementsByClassName(
              "highlight-in-h1"
            )[0].innerText;
            console.log("TotalProjects:", totalProjects);
            let projectCount = parseInt(totalProjects);
            let divider = parseInt(limit);
            let totalPages = Math.ceil(projectCount / divider);

            return {
              totalPages: totalPages,
              totalProjects: totalProjects
            };
          },
          SELECTORS,
          limit
        );

      console.log("Total link count:", pagination.totalProjects);

      // iterate through pagination
      for (let i = 0; i < pagination.totalPages; i++) {
        console.log("Iteration phase:", i + 1);
        console.log("Offsetvalue:", offset + i * limit);
        let offsetValue = offset + i * limit;
        url = baseURL + _query(offsetValue, limit);
        page.setJavaScriptEnabled(false);
        try {
          await page.goto(url, { waitUntil: "domcontentloaded" });
        } catch (error) {
          console.log(CHALK.errMsg("Error fetching page:", error));
          console.log(CHALK.warnMsg("Retrying url:", url));
        }

        let links = await page.evaluate(SELECTORS => {
          let nodes = [...document.querySelectorAll(SELECTORS.projectLink)];
          let linksArray = [];
          nodes.map(node => {
            let route = node.href.split(
              "https://www.salto-youth.net/tools/otlas-partner-finding/"
            )[1];
            console.log(
              node.href.split(
                "https://www.salto-youth.net/tools/otlas-partner-finding/"
              )
            );
            linksArray.push(route);
          });

          return linksArray;
        }, SELECTORS);
        console.log("Found links:", projectLinks.length);
        projectLinks = projectLinks.concat(links);
        console.log(CHALK.warnMsg("Updated links:", projectLinks.length));
      }

      console.log("Found links:", projectLinks);

      await browser.close();

      console.log(CHALK.infoMsg("Browser instance closed."));

      return projectLinks;
    } catch (error) {
      console.log(CHALK.errMsg(error));
      return new Meteor.Error(error);
    }
  };

  getDetails = async link => {
    let start = process.hrtime();
    let end = process.hrtime(start);
    const browser = await puppeteer.launch(_puppeteerOptions);
    try {
      console.log(CHALK.infoMsg("Getting project details:"), link);

      const page = await browser.newPage();

      console.log(CHALK.success("New browser instance started"));

      await page.goto(link, { waitUntil: "domcontentloaded" });
      console.log(CHALK.success("DOM Content Loaded"));

      await page.waitFor(2000);

      console.log(CHALK.infoMsg("Getting project details."));
      let collection = await page.evaluate(SELECTORS => {
        console.log(SELECTORS);
        let projectTitle = () => {
          let title = document.querySelector(SELECTORS.projectTitle);

          return title ? title.innerText : "noTitle";
        };

        let projectSummary = () => {
          let summary = document.querySelector(SELECTORS.projectSummary);

          return summary ? summary.innerText : "noSummary";
        };
        let projectDescription = () => {
          let description = document.querySelector(
            SELECTORS.projectDescription
          );

          if (description) {
            return Array.from(description.children)
              .map(p => p.innerText)
              .join("\n");
          } else {
            return "noDescription";
          }
        };

        let projectClosed = () => {
          let closed = document.querySelector(SELECTORS.projectClosed);

          return closed ? true : false;
        };

        let partnerCount = () => {
          let count = document.querySelector(SELECTORS.partnerCount);
          if (count) {
            return parseInt(count.innerText);
          } else {
            return "noPartnersNeeded";
          }
        };

        let partnersFrom = () => {
          let from = document.querySelector(SELECTORS.partnersFrom);

          return from !== null && from.innerText.includes("from")
            ? from.innerText.split("from ")[1].split(", ")
            : "noFrom";
        };

        let projectDeadline = () => {
          let deadline = document.querySelector(SELECTORS.projectDeadline);

          return deadline !== null
            ? JSON.stringify(new Date(
                deadline.innerText.split("-").map(item => parseInt(item))
              ))
            : "noDeadline";
        };

        let contactDetails = () => {
          let contact = document.querySelector(SELECTORS.contactDetails);



          return contact !== null
            ? Array.from(contact.children).map(p => p.innerText)
            : "noContactDetails";
        };

        let organizationName = () => {
          let orgName = document.querySelector(SELECTORS.organizationName);
          return orgName !== null ? orgName.innerText : "noOrganizationName";
        };

        let organizationLink = () => {
          let orgLink = document.querySelector(SELECTORS.organizationLink);
          return orgLink ? orgLink.href : "noOrganizationLink";
        };

        let projectStartDate = () => {
          let dates = document.querySelector(SELECTORS.projectDates);

          if (dates.innerText !== null)
            {
              let projectStartDate = JSON.stringify(new Date(dates.innerText.split("from ")[1].split(" till ")[0]));
              return (projectStartDate)
            }
            else {return "noStartDate"};
        };

        let projectEndDate = () => {
          let dates = document.querySelector(SELECTORS.projectDates);

          return dates.innerText !== null
            ? JSON.stringify(new Date(dates.innerText.split("from ")[1].split(" till ")[1]))
            : "noEndDate";
        };

        let projectRelation = () => {
          let relation = document.querySelector(SELECTORS.projectRelation);

          return relation !== null
            ? relation.innerText
                .split("This project relates to:\n")[1]
                .split(", ")
            : "noRelations";
        };

        let projectFocus = () => {
          let focus = document.querySelector(SELECTORS.projectFocus);
          return focus !== null ? focus.innerText.split("\n") : "noFocus";
        };

        let projectIncludes = () => {
          let includes = document.querySelector(SELECTORS.projectIncludes);

          return includes !== null
            ? includes.innerText.split("\n")
            : "noInclude";
        };

        let projectIncludesDescription = () => {
          let incDetails = document.querySelector(
            SELECTORS.projectIncludesDescription
          );

          return incDetails !== null
            ? incDetails.innerText
            : "noIncludeDescription";
        };

        let projectShortLink = document.getElementsByClassName("short-url")[0]
          .innerText;

        return new Promise(resolve => {
          resolve({
            projectTitle: projectTitle() || "noTitle",
            projectSummary: projectSummary() || "noSummary",
            projectDescription: projectDescription() || "noDescription",
            partnerCount: partnerCount() || "noPartner",
            partnersFrom: partnersFrom() || "noFrom",
            projectDeadline: projectDeadline() || "noDeadline",
            contactDetails: contactDetails() || "noContact",
            organizationName: organizationName() || "noOrganization",
            organizationLink: organizationLink() || "noOrganizationLink",
            projectStartDate: projectStartDate() || "noStartDate",
            projectEndDate: projectEndDate() || "noEndDate",
            projectIncludes: projectIncludes() || "noInclude",
            projectIncludesDescription:
              projectIncludesDescription() || "noIncludeDescription",
            projectRelation: projectRelation() || "noRelation",
            projectFocus: projectFocus() || "noFocus",
            projectShortLink: projectShortLink || "noShortLink",
            projectClosed: projectClosed(),
            createdAt: JSON.stringify(new Date())
          });
        });
      }, SELECTORS);
      console.log(CHALK.success("Project details saved."));

      await page.goto(collection.organizationLink, {
        waitUntil: "networkidle2"
      });

      await page.waitFor(1000);

      console.log(CHALK.infoMsg("Getting organization details."));
      let organizationDetails = await page.evaluate(SELECTORS => {
        let details = document.getElementsByClassName(
          "tool-item-meta organisation-meta"
        )[0];
        if (details) {
          let organizationType = details.childNodes[1].innerText;
          let organizationBase = details.childNodes[5].innerText;
          console.log(organizationType, organizationBase);
          return {
            type: organizationType,
            base: organizationBase
          };
        } else {
          return null;
        }
      }, SELECTORS);

      console.log(CHALK.success("Organization details saved."));

      await browser.close();
      collection.organizationDetails = organizationDetails;
      console.log(CHALK.success("All project details saved!"));
      console.log(CHALK.warnMsg("Done in"), ` ${end[1] / 1000} seconds`);
      return new Promise((resolve, reject) => {
        resolve(collection);
      });
    } catch (error) {
      await browser.close();
      console.dir(CHALK.errMsg(error));
      return("Error:", error);
    }
  };
}

const puppet = new Puppeteer();

export default puppet;
