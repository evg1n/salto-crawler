import { Base64 } from "js-base64";

const { google } = require("googleapis");

import * as CHALK from "../startup/server/chalk.js";

const TOKEN = Meteor.settings.google_api_token;



class GMail {
  authorize(credentials) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id,
      client_secret,
      redirect_uris[0]
    );

    oAuth2Client.setCredentials(TOKEN);
    return oAuth2Client;
  }

  listLabels(auth) {
    const gmail = google.gmail({ version: "v1", auth });
    gmail.users.labels.list(
      {
        userId: "me"
      },
      (err, res) => {
        if (err) return console.log("The API returned an error: " + err);
        const labels = res.data.labels;
        if (labels.length) {
          console.log("Labels:");
          labels.forEach(label => {
            console.log(`- ${label.name}`);
          });
        } else {
          console.log("No labels found.");
        }
      }
    );
  }

  getInboxList(auth) {
    const gmail = google.gmail({ version: "v1", auth });

    let request = gmail.users.messages.list({
      userId: "me"
    });

    let messages = request
      .then(res => { console.log("Response:",res.data.messages); return res.data.messages})
      .catch(error => console.error("Error getting inbox:", error));

      console.dir("Messages:", messages)

    return messages;
  }

  async getMessages(auth, threadIdArray) {
    const gmail = google.gmail({ version: "v1", auth });
    let messages = [];
    for (let thread of threadIdArray) {
      await gmail.users.messages
        .get({ userId: "me", id: thread.id })
        .then(response => {
          let payload = response.data.payload;

          let headers = {};

          for (let header of payload.headers) {
            switch (header.name) {
              case "Received":
                let value = header.value.split(";")[1];
                headers.received = value.trim();
              case "Subject":
                headers.subject = header.value;
              case "From":
                headers.from = header.value;

              case "To":
                headers.to = header.value;
              default:
                break;
            }
          }

          let body = {};

          if (payload.parts) {
            for (let part of payload.parts) {
              switch (part.mimeType) {
                case "text/plain":
                  body.text = Base64.decode(part.body.data);
                //case "text/html":
                //  body.html = Base64.decode(part.body.data);
                default:
                  break;
              }
            }
          } else {
            body.text = Base64.decode(payload.body.data);
          }
          headers.threadId = thread.id;
          messages.push({ body: body, headers: headers });
        })
        .catch(err => {
          console.error("Error retrieving messages:", err);
        });
    }
    console.log("Retrieved messages!");
    return messages;
  }

  async mailSend(auth, emailObject) {
    console.log("Sending Mail To:", emailObject.to, "Subject:", emailObject.subject);
    const gmail = google.gmail({ version: "v1", auth });
    const { to, from, subject, message, references, inReplyTo,type } = emailObject;
    const utf8Subject = `=?utf-8?B?${Buffer.from(subject).toString(
      "base64"
    )}?=`;

    const messageParts = [
      `From: ${from}`,
      `To: ${to}`,
      "Content-Type: text/html; charset=utf-8",
      "MIME-Version: 1.0",
      `Subject: ${utf8Subject} - Partnership Request`,
      "",
      `${message}`
    ];

     if (references && inReplyTo) {
    messageParts.unshift(`References: ${references}`, `In-Reply-To: ${id}`)
    }
    const messageBody = messageParts.join("\n");

    // The body needs to be base64url encoded.
    const encodedMessage = Buffer.from(messageBody)
      .toString("base64")
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/=+$/, "");

      if (type !== "expired") {
        let request = await gmail.users.messages.send({
          userId: "me",
          requestBody: {
            raw: encodedMessage,
          },
        });
        console.log(CHALK.success("Mail sent:"), request.data);

        return request;
      } else {
        console.log(CHALK.errMsg("Mail not sent!"))
        return {
          data: {
            threadId: "expired",
          },
        };
      }
    
  }

  async getThread(auth, threadId){
    const gmail = google.gmail({ version: "v1", auth });
    let thread = gmail.users.threads.get({
      "userId": "me",
      "id": threadId
    })
    return thread
  }
}

const gmailController = new GMail();

export default gmailController;


