import MainData from "../startup/both/collections/mainData.js";
import { Checkbox } from "semantic-ui-react";

class SearchController {
  constructor() {}

  async searchExactMatch(searchQuery, skip, limit) {
    check(searchQuery, Object);



    // prevent frequent execution

    try {
      let tags = searchQuery.searchFilters.map(item =>
        item.length > 0 ? `"${item}"` : ""
      );
      let serializedQuery =
        searchQuery.searchInput.length > 0
          ? tags.concat(`"${searchQuery.searchInput}"`).join(" ")
          : tags.join(" ");

          console.log("query:", serializedQuery)
      let results = MainData.find(
        {
          $text: {
            $search: `\"${serializedQuery}\"`,
            $caseSensitive: true,
            $diacriticSensitive: true
          }
        },

        { skip: skip, limit: limit, sort: {projectEndDate: -1}}
      ).fetch();

      let resultCount = MainData.find({
        $text: {
          $search: `\"${serializedQuery}\"`,
          $caseSensitive: true,
          $diacriticSensitive: true
        }
      }).count();
      return {
        resultCount: resultCount,
        results: results
      };
    } catch (error) {
      return new Meteor.Error("Search Error:", error);
    }
  }

  async getAllProjects (skip, limit) {
    console.log("skip:", skip, "limit:", limit)

    let totalProjects = await MainData.find({projectDescription: {$exists: true}}).count()

    let projects = await MainData.find({ projectTitle: { $exists: true }}, {sort:{publishedAt: -1}, skip: skip, limit: limit, fields:{projectTitle: 1, projectShortLink: 1, organizationName: 1, contactDetails: 1, projectRelation: 1, organizationLink: 1}}).fetch();
    

    return {
      totalProjects: totalProjects,
      projects: projects
    }

  }

  async findOne (projectId){
    return await MainData.findOne({_id: projectId})
  }
}

const searchController = new SearchController();

export default searchController;
