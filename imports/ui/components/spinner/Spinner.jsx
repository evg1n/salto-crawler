import React from 'react'
import { Loader } from "semantic-ui-react";


const Spinner = (props) => {
  return (
  <Loader inline="centered" active={props.active}>
  {props.text}</Loader>
  )
}

export default Spinner
