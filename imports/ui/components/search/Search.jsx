import React, { useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";

import { Input, Button, Form, Popup } from "semantic-ui-react";
import {
  changeSearchFilter,
  changeSearchInput,
  changeTotalProjects
} from "../../redux";
import searchReducer from "../../redux/search/searchReducer";
const Search = props => {
  const [loading, toggleLoading] = useState(false);
  const [searchFocus, toggleFocus] = useState(false);
  const [value, setValue] = useState("");

  const searchFilters = useSelector(state => state.search.searchFilters);

  const dispatch = useDispatch();

  return (
    <div className="flex pa2 ba br2 b--black-20 w-100 items-center justify-center">
      <Form className="w-70">
        <Form.Group style={{ margin: 0 }}>
          <Form.Input
            width={10}
            onFocus={() => toggleFocus(true)}
            onBlur={() => {
              toggleFocus(false);
            }}
            focus={searchFocus}
            loading={props.isLoading || loading}
            placeholder="Arama"
            className="w-50 mh2"
            onChange={(event, data) => setValue(data.value)}
          />
          <Button
            onClick={() => dispatch(changeSearchInput(value))}
            icon="search"
            content="Ara"
          />

          <div>
            <Popup
              on="click"
              position="bottom center"
              wide="very"
              hoverable
              trigger={<Button icon="filter" content="Filtre" />}
            >
              <Form.Field>
                <Form.Checkbox
                  width="sixteen"
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  value="Youth Exchanges"
                  label="Youth Exchanges"
                  checked={searchFilters.includes("Youth Exchanges")}
                />
                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Volunteering Activities (formerly EVS)"
                  value="Volunteering Activities (formerly EVS)"
                  checked={searchFilters.includes(
                    "Volunteering Activities (formerly EVS)"
                  )}
                />

                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Training and Networking"
                  value="Training and Networking"
                  checked={searchFilters.includes("Training and Networking")}
                />
                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Transnational Youth Initiatives"
                  value="Transnational Youth Initiatives"
                  checked={searchFilters.includes(
                    "Transnational Youth Initiatives"
                  )}
                />

                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Strategic Partnerships"
                  value="Strategic Partnerships"
                  checked={searchFilters.includes("Strategic Partnerships")}
                />

                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Capacity Building"
                  value="Capacity Building"
                  checked={searchFilters.includes("Capacity Building")}
                />
                <Form.Checkbox
                  onChange={(event, data) =>
                    dispatch(changeSearchFilter(data.value))
                  }
                  width="sixteen"
                  label="Meetings between young people and decision-makers"
                  value="Meetings between young people and decision-makers"
                  checked={searchFilters.includes(
                    "Meetings between young people and decision-makers"
                  )}
                />
              </Form.Field>
            </Popup>
          </div>
        </Form.Group>
      </Form>
      <div className="w-30">
        <p>
          Arama sonucunda bulunan proje sayısı:{" "}
          <strong>{props.totalProjects}</strong>
        </p>
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    searchInput: state.search.searchInput,
    searchFilters: state.search.searchFilters,
    totalProjects: state.pagination.totalProjects
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeSearchInput: input => dispatch(changeSearchInput(input)),
    changeSearchFilter: input => dispatch(changeSearchFilter(input)),
    changeTotalProjects: inptu => dispatch(changeTotalProjects(input))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Search);
