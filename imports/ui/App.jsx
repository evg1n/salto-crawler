//import modules
import { useTracker } from "meteor/react-meteor-data";
import React, { useState, useEffect } from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

// import mongo collection
import MainData from "../startup/both/collections/mainData.js";

//redux store
import store from "./redux/store.js";

//import styles
import tachyons from "tachyons";
import "semantic-ui-css/semantic.min.css";
import "./App.css";

//import components
import Projects from "./containers/projects/Projects.jsx";
import Spinner from "./components/spinner/Spinner.jsx";

import Crawler from "./containers/crawler/Crawler.jsx";
import ProjectPagination from "./containers/projects/components/ProjectPagination.jsx";
import Overview from "./containers/projects/components/ProjectOverview.jsx";
import Search from "./components/search/Search.jsx";

const subscription = (skip, limit) =>
  useTracker(() => {
    const sub = Meteor.subscribe("mainData", skip, limit);
    return !sub.ready();
  }, []);

const mainData = (skip, limit, searchQuery) => {
  if (
    searchQuery.searchInput.length < 1 &&
    searchQuery.searchFilters.length < 1
  ) {
    console.log("I will not search");
    return MainData.find(
      { projectTitle: { $exists: true } },
      { skip: skip, limit: limit }
    ).fetch();
  } else {
    console.log("I will search.");
    Meteor.call("search", searchQuery, skip, limit, (error, results) => {
      if (error) {
        console.error("Search error:", error);
        return [];
      }
      console.log("Search results:", results);
      return results;
    });
  }
};

const App = () => {
  const [totalProjects, setTotalProjects] = useState(0);
  const [totalPages, setTotalPages] = useState(1);
  const isLoading = subscription(0, 100);
  const projects = (skip, searchQuery) => mainData(skip, 100, searchQuery);
  const defaultActivePage = store.getState().pagination.activePage;
  const skip = defaultActivePage * 100 - 100;
  const searchQuery = () => JSON.stringify(store.getState().search);
  const [results, getResults] = useState();

  useEffect(() => {
    console.log("query change", searchQuery);
  }, [searchQuery()]);

  const getProjects = async (searchQuery, skip, limit) => {
    if (
      searchQuery.searchInput.length < 1 ||
      searchQuery.searchFilters.length < 1
    ) {
      let results = MainData.find(
        { projectTitle: { $exists: true } },
        { skip: skip, limit: limit }
      ).fetch();

      getResults(results);
    } else {
      Meteor.call("search", searchQuery, skip, limit, (error, results) => {
        if (error) {
          console.log("Search error:", error);
        }

        getResults(results);
      });
    }
  };

  useEffect(() => {
    console.log("effect", isLoading, searchQuery);
    if (!isLoading) {
      let projectCount = MainData.find().count();
      let pageCount = Math.ceil(projectCount / 100);
      setTotalProjects(projectCount);
      setTotalPages(pageCount);
    }
    getProjects(searchQuery, skip, 100)
  }, [isLoading, searchQuery]);

  return (
    <Router>
      <Provider store={store}>
        <div className="flex flex-wrap items-start w-100 pa2 vh-100">
          <Switch>
            <Route exact path="/">
              <div className="w-100">
                <Crawler />
              </div>
              <div className="w-100">
                <Search isLoading={isLoading} totalProjects={totalProjects} />
              </div>
              <div className="w-100">
                {isLoading ? null : (
                  <div className="flex w-100 justify-center pa2">
                    <ProjectPagination totalPages={totalPages} />
                  </div>
                )}

                {isLoading ? (
                  <Spinner
                    active={isLoading}
                    text={"Veri bekleniyor"}
                    inline="centered"
                    className="w-100 ma2 pa2"
                  />
                ) : (
                  <div className="project-container ba br2 b--black-20 overflow-y-scroll shadow-1">
                    <Projects
                      activePage={defaultActivePage}
                      results={results}
                    />
                  </div>
                )}
              </div>
            </Route>

            <Route exact path="/projects/overview">
              <div>
                <Overview />
              </div>
            </Route>

            <Route>
              <div>
                <p>Not found</p>
              </div>
            </Route>
          </Switch>

          <div className="w-100 tc">
            <h6>
              ©2020 Kıyı Doğa Sporları Kulübü Derneği. Tüm hakları saklıdır.
            </h6>
          </div>
        </div>
      </Provider>
    </Router>
  );
};

export default App;

//<Register className="w-100" />;
