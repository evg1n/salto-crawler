export {
changeEmail,
changePassword,
changeConfirmPassword
}from "./register/registerActions.js";

export {
  changePagination,
  changeTotalPages,
  changeTotalProjects
} from "./pagination/paginationActions.js";

export {
  changeSearchInput,
  changeSearchFilter
} from "./search/searchActions.js";

export {
  toggleLoading
} from "./loading/loading.js";