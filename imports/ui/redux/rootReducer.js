import { combineReducers } from "redux";
import registerReducer from "./register/registerReducer.js";
import paginationReducer from "./pagination/paginationReducer.js"
import searchReducer from "./search/searchReducer.js";
import loadingReducer from "./loading/loading.js";

const rootReducer = combineReducers({
  register: registerReducer,
  pagination: paginationReducer,
  search: searchReducer,
  loading: loadingReducer
});

export default rootReducer;
