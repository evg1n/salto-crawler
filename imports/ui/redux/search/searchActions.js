import {
SEARCH_INPUT_CHANGE,
SEARCH_FILTER_CHANGE
} from "./searchTypes.js";

export const changeSearchInput = input => {
  return {
    type: SEARCH_INPUT_CHANGE,
    payload: input
  };
};

export const changeSearchFilter = input => {
  return {
    type: SEARCH_FILTER_CHANGE,
    payload: input
  };
};

