import {
SEARCH_INPUT_CHANGE,
SEARCH_FILTER_CHANGE
} from "./searchTypes.js";

const initialState = {
  searchInput: "",
  searchFilters:[]
};

const searchReducer = (state = initialState, action) => {

  let { payload } = action



  switch (action.type) {
    case SEARCH_INPUT_CHANGE:
      return {
        ...state,
        searchInput: payload
      };

    case SEARCH_FILTER_CHANGE:

      let filters = [...state.searchFilters];

      let itemIndex = filters.indexOf(payload);

      if (itemIndex < 0){
        filters.push(payload)
      } else {

        filters.splice(itemIndex, 1)
      }
      return {
        ...state,
        searchFilters: filters
      };
    default:
      return state;
  }
};

export default searchReducer;
