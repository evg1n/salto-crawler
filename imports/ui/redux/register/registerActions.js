import {
  INPUT_EMAIL,
  INPUT_PASSWORD,
  INPUT_CONFIRM_PASSWORD
} from "./registerTypes.js";


export const changeEmail = (input) => {
  console.log(input)
  return {
    type: INPUT_EMAIL,
    payload: input
  };
};

export const changePassword = (input) => {
  console.log(input);
  return {
    type: INPUT_PASSWORD,
    payload: input
  };
};

export const changeConfirmPassword = (input) => {
  console.log(input);
  return {
    type: INPUT_CONFIRM_PASSWORD,
    payload: input
  };
};