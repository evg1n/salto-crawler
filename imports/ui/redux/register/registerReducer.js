import {
  INPUT_EMAIL,
  INPUT_PASSWORD,
  INPUT_CONFIRM_PASSWORD
} from "./registerTypes.js";

const initialState = {
  email: "",
  password: "",
  confirmPassword: ""
};

const registerReducer = (state = initialState, action) => {
  switch (action.type) {
    case INPUT_EMAIL:
      return {
        ...state,
        email: action.payload
      };

    case INPUT_PASSWORD:
      return {
        ...state,
        password: action.payload
      };

    case INPUT_CONFIRM_PASSWORD:
      return {
        ...state,
        confirmPassword: action.payload
      };

    default:
      return state;
  }

  
};

export default registerReducer;
