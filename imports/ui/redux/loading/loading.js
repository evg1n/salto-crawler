const TOGGLE_LOADING = "TOGGLE_LOADING";

const initialState = {
  isLoading: true
}
//actions
export const toggleLoading = (input) => {
  return {
    type: TOGGLE_LOADING,
    payload: input
  }
}

//reducers

const loadingReducer = (state = initialState, action) => {

  if (action.type === TOGGLE_LOADING) {
    return {
      ...state,
      isLoading: action.payload
    }
  } else {
    return state
  }

}

export default loadingReducer