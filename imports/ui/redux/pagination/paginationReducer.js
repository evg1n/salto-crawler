import { 
  PAGINATION_CHANGE,
  PAGINATION_TOTALPAGE_CHANGE,
  PAGINATION_TOTAL_PROJECTS_CHANGE
} from "./paginationTypes.js";

const initialState = {
  activePage: 1,
  totalPages: 1,
  totalProjects: 0
};

const paginationReducer = (state = initialState, action) => {
  switch (action.type) {
    case PAGINATION_CHANGE:
      return {
        ...state,
        activePage: action.payload
      };

    case PAGINATION_TOTALPAGE_CHANGE:
      return {
        ...state,
        totalPages: action.payload
      };
    case PAGINATION_TOTAL_PROJECTS_CHANGE:
      return {
        ...state,
        totalProjects: action.payload
      };

    default:
      return state;
  }
};

export default paginationReducer;
