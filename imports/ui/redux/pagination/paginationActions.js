import {
  PAGINATION_CHANGE,
  PAGINATION_TOTALPAGE_CHANGE,
  PAGINATION_TOTAL_PROJECTS_CHANGE
} from "./paginationTypes.js";

export const changePagination = input => {
  return {
    type: PAGINATION_CHANGE,
    payload: input
  };
};

export const changeTotalPages = input => {
  return {
    type: PAGINATION_TOTALPAGE_CHANGE,
    payload: input
  }
}

export const changeTotalProjects = input => {
  return {
    type: PAGINATION_TOTAL_PROJECTS_CHANGE,
    payload: input
  };
};