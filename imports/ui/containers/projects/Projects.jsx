import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { changeTotalPages, toggleLoading, changeTotalProjects } from "../../redux/index.js";

import { Table, TableBody } from "semantic-ui-react";

import ProjectOverview from "./components/ProjectOverview.jsx";
import Spinner from "../../components/spinner/Spinner.jsx";


// TODO: Pagination değiştiğinde search değil tüm projeleri çağıran fonksiyonu çağırıyor


const Projects = props => {
  const activePage = useSelector(state => state.pagination.activePage);
  const isLoading = useSelector(state => state.loading.isLoading);
  const searchQuery = useSelector(state => state.search);
  const [projects, getProjects] = useState([]);

  const [skip, setSkip] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(toggleLoading(true));
    console.log("useeffect");
    const skip = activePage * 100 - 100;
    async function getAllProjects() {
      console.log("getAllProjects");
      await Meteor.call(
        "getAllProjects",
        searchQuery,
        skip,
        100,
        (error, results) => {
          if (error) {
            console.log("Projects error:", error);
          }
          console.log("results:", results);
          dispatch(changeTotalPages(Math.ceil(results.totalProjects / 100)));
          dispatch(changeTotalProjects(results.totalProjects));
          getProjects(results.projects);
          dispatch(toggleLoading(false));
        }
      );
    }
    const debouncedGetAllProjects = _.debounce(getAllProjects, 500);
    setSkip(skip);
    if (
      searchQuery.searchInput.length < 1 &&
      searchQuery.searchFilters.length < 1
    ) {
      console.log("triggered all projects")
      debouncedGetAllProjects();
    }
    
  }, [activePage, searchQuery]);

  useEffect(() => {
    dispatch(toggleLoading(true));
    const skip = activePage * 100 - 100;
    async function search(searchQuery, skip, limit) {
      console.log("searching");
      await Meteor.call(
        "search",
        searchQuery,
        skip,
        limit,
        (error, results) => {
          if (error) {
            console.log("Search error:", error);
            return getProjects([]);
          }
          console.log("search results:", results.resultCount);

          dispatch(changeTotalPages(Math.ceil(results.resultCount / 100)));
          dispatch(changeTotalProjects(results.resultCount))
          dispatch(toggleLoading(false));
          return getProjects(results.results);
        }
      );
    }
    const debouncedSearch = _.debounce(search, 100);
    if (
      searchQuery.searchInput.length > 0 ||
      searchQuery.searchFilters.length > 0
    ) {
      console.log("triggered search")
      debouncedSearch(searchQuery, skip, 100);
    }
  }, [searchQuery, activePage]);

  return (
    <div>
      <Table celled striped>
        <Table.Header fullWidth={true}>
          <Table.Row>
            <Table.HeaderCell colSpan={6} textAlign="center">
              SALTO PROJE GENEL GÖRÜNÜMÜ
            </Table.HeaderCell>
          </Table.Row>
          <Table.Row>
            <Table.HeaderCell textAlign="center">No</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">
              Proje Başlığı
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="center">
              Organizasyon Adı
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="center">
              E-Posta Adresi
            </Table.HeaderCell>
            <Table.HeaderCell textAlign="center">Menü</Table.HeaderCell>
            <Table.HeaderCell textAlign="center">
              İlgili Alanlar
            </Table.HeaderCell>
          </Table.Row>

          {isLoading ? (
            <Table.Row>
              <Table.HeaderCell colSpan={6} textAlign="center">
                <Spinner
                  colSpan={6}
                  active={true}
                  text={"Veri bekleniyor"}
                  inline="centered"
                  className="w-100 ma2 pa2"
                />
              </Table.HeaderCell>
            </Table.Row>
          ) : null}
        </Table.Header>
        {isLoading ? null : (
          <Table.Body>
            {typeof projects === "object"
              ? projects.map((project, index) => (
                  <ProjectOverview
                    key={"project-overview-" + index}
                    index={index}
                    skip={skip}
                    project={project}
                  />
                ))
              : null}
          </Table.Body>
        )}
      </Table>
    </div>
  );
};

export default Projects;
