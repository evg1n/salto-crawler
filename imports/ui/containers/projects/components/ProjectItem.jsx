import React, { useState, useEffect } from "react";

import { Loader, Dimmer } from "semantic-ui-react";

export const ProjectItem = props => {
  const [loading, setLoading] = useState(true);
  const [project, displayProject] = useState({});

  const projectId = props.data._id;

  useEffect(() => {
    async function getProject(projectId) {
      Meteor.call("findOne", projectId, (error, result) => {
        if (error) {
          return console.log("Error:", error);
        }
         console.log("result", result);
        displayProject(result);
        setLoading(false)
       
      });
    }
    getProject(projectId);
  }, []);

  const {
    projectTitle,
    projectSummary,
    projectShortLink,
    projectDescription,
    projectDeadline,
    projectStartDate,
    projectEndDate,
    organizationName,
    organizationLink,
    organizationDetails,
    partnerCount,
    partnersFrom,
    projectFocus,
    contactDetails,
    projectIncludes,
    projectRelation,
    publishedAt
  } = project;

  return (
    <div className="w-100 ma2 pa2 br2 ba b--black-20">
      {loading ? (
        <div className="vh-25">
          <Dimmer active inverted>
            <Loader active inline="centered" />
          </Dimmer>
        </div>
      ) : (
        <div>
          <div>
            <h3>{projectTitle}</h3>
            <p>
              <strong>Project Summary: </strong>
              {projectSummary}
            </p>
            <p>
              <strong>Description: </strong>
              {projectDescription}
            </p>
            <p>
              <strong>Partners Needed: </strong>
              {partnerCount}
              <strong> From: </strong>
              {typeof partnersFrom === "object"
                ? partnersFrom.join(", ")
                : partnersFrom}
            </p>
            <p>
              <strong>Until: </strong>
              {projectDeadline === "noDeadline"
                ? projectDeadline
                : projectDeadline.toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                  })}
              <strong> Start Date: </strong>
              {typeof projectStartDate === "string"
                ? projectStartDate
                : projectStartDate.toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                  })}
              <strong> End Date: </strong>
              {projectEndDate === "noEndDate"
                ? projectEndDate
                : projectEndDate.toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                  })}
              <strong> Project Published: </strong>
              {publishedAt.toLocaleDateString("en-US", {
                    year: "numeric",
                    month: "long",
                    day: "numeric"
                  })}
            </p>
            <p>
              <em>Project Link: </em>
              <a href={projectShortLink}>{projectShortLink}</a>
            </p>
            <div>
              <h4 style={{ margin: 0 }}>Contact Details:</h4>
              <ul style={{ marginTop: 0 }}>
                {contactDetails.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            </div>
          </div>

          <div>
            <h4>Organization Details:</h4>
            <p>
              <strong>Organization Name: </strong>
              {organizationName}

              <strong> Organization Based In: </strong>
              {organizationDetails
                ? organizationDetails.base
                : "noOrganization base"}
            </p>
            <p>
              <strong>Organization Link: </strong>
              <a href={organizationLink}>{organizationLink}</a>
            </p>
            <p>
              <strong>Organization Type: </strong>
              {organizationDetails
                ? organizationDetails.type
                : "noOrganization type"}
            </p>

            <div>
              <p>
                <strong>This project includes:</strong>
              </p>
              <ul style={{ marginTop: 0 }}>
                {typeof projectIncludes !== "string"
                  ? projectIncludes.map((item, index) => (
                      <li key={index}>{item}</li>
                    ))
                  : "-"}
              </ul>

              <p>
                <strong>This project focuses on: </strong>
                {typeof projectFocus === "object"
                  ? projectFocus.join(", ")
                  : "noFocus"}
              </p>
              <p>
                <strong>This project relates to: </strong>
                {typeof projectRelation === "object"
                  ? projectRelation.join(", ")
                  : "noRelation"}
              </p>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default ProjectItem;
