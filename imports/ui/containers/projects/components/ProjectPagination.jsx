import React, { useEffect } from "react";
import { Pagination } from "semantic-ui-react";
import { connect, useDispatch, useSelector } from "react-redux";

import { changePagination, changeTotalPages } from "../../../redux/index.js";
import ProjectOverview from "./ProjectOverview.jsx";

const ProjectPagination = props => {
  const dispatch = useDispatch();

  return (
    <Pagination
      ellipsisItem={null}
      totalPages={props.totalPages}
      defaultActivePage={props.activePage}
      boundaryRange={2}
      onPageChange={(event, data) =>
        dispatch(changePagination(data.activePage))
      }
    />
  );
};

const mapStateToProps = state => {
  return {
    activePage: state.pagination.activePage,
    totalPages: state.pagination.totalPages
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changePagination: input => dispatch(changePagination(input))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectPagination);
