import React, {useState} from 'react'

export const Template = () => {

  const [html, setHtml] = useState("");

  const getHtml = () => {
    Meteor.call("renderTemplate", "Proje Adı", "Yetkili Kişi", "https://www.kiyidosk.org/", (error, result)=> {
      return error 
      ? console.log("Error HTML:", error)
      : setHtml(result)
    })
  }
  return (
    <div>
    <button onClick={getHtml}>Get HTML</button>
      {html}
    </div>
  )
}
