import React, {useEffect, useState} from 'react'

import { Loader, Dimmer } from "semantic-ui-react";
 import Message from "./Message.jsx"



const GMail = (props) => {

    const [messages, setMessages] = useState([]);
    const [messageBody, setBody] = useState("");
    const [fetch, setFetch] = useState(false);

  const getThread = (shortLink) => {
    Meteor.call("getThread", shortLink, (error, response) => {
      if (error) {
        setFetch(true);
        return console.log("Thread error:", error);
      }
      console.log("Mails:", response);

      setMessages(response[0].messages);
      setFetch(true);
    });
  }

  useEffect(()=> {
    console.log("Triggered getThread", messages, props)
    getThread(props.shortLink);
  }, [])

  

  return (
    <div className="w-100 pa3">
      {messages.length > 0 ? (
        messages.map((item, index) => {
          console.log("message:", item);
          return <Message key={"message-" + index} message={item} />;
        })
      ) : (
        <div className="vh-25">
          {
            fetch ? (
            <p>Hiç e-posta yok.</p>
          ) : (
            <Dimmer active inverted>
              <Loader active inline="centered" />
            </Dimmer>
          )}
        </div>
      )}
    </div>
  );
}

export default GMail;