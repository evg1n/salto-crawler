import React, { useState, useEffect } from "react";
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2
} from "react-html-parser";
import _ from "lodash";
import { Base64 } from "js-base64";

import { Container, Button, Modal, TextArea } from "semantic-ui-react";
import { contactDetails } from "../../../../../startup/both/lib";

import Spinner from "../../../../components/spinner/Spinner.jsx";

const Message = props => {
  const { headers, body, parts } = props.message.payload;
  const [messageHeaders, setMessageHeaders] = useState({});
  const [messagebody, setMessageBody] = useState("");
  const [replyBody, setReplyBody] = useState("");
  const [isReply, toggleIsReply] = useState("");
  const [open, setOpen] = useState(false);

  const transform = (node, index) => {
    if (node.type === "tag" && node.name === "template") {
      node.name = "div";
      return console.log("template!");
    }
  };

  const options = {
    decodeEntities: true,
    transform
  };

  useEffect(() => {
    let newHeaders = {};
    headers.map(header => {
      switch (header.name) {
        case "To":
          newHeaders.to = header.value;
          break;
        case "Subject":
          newHeaders.subject = header.value;
          break;
        case "Date":
          newHeaders.date = header.value;
          break;
        case "From":
          newHeaders.from = header.value;
          break;
        case "Message-ID":
          newHeaders.id = header.value;
          break;
        case "References":
          newHeaders.references = header.value;
        case "In-Reply-To":
          newHeaders.inReplyTo = header.value;
          toggleIsReply(true);
          break;
        default:
          break;
      }
      let currentHeaders = [];

      if (Object.keys(newHeaders).length > 0) {
        currentHeaders.push(newHeaders);
      }
    });
    setMessageHeaders(newHeaders);

    if (body.size === 0) {
      console.log("Message Body", body);

      if (parts[0].body.size === 0) {
        setMessageBody(Base64.decode(parts[0].parts[0].body.data));
      } else {
        setMessageBody(Base64.decode(parts[0].body.data));
      }
    } else {
      console.log("Message body:", parts);
      setMessageBody(Base64.decode(body.data));
    }
  }, []);

  const sendReply = (messageHeaders, replyBody) => {
    let emailObject = messageHeaders;

    emailObject.message = replyBody;

    console.log(emailObject);

    Meteor.call("sendMail", emailObject, (error, result) =>
      error
        ? console.log("Error reply:", error)
        : console.log("Success:", result)
    );
  };

  return (
    <div className={"w-100 pa2 mv2 ba br2 b--blue shadow-5"}>
      <Container fluid text textAlign="justified">
        <div className="flex items-center">
          <p>
            <strong>Date:</strong> {messageHeaders.date} <br />
            <strong>Subject:</strong> {messageHeaders.subject}
            <br />
            <strong>From:</strong> {messageHeaders.from}
            <br />
            <strong>To:</strong> {messageHeaders.to}
            <br />
          </p>
          {messageHeaders.inReplyTo ? (
            <div className="pa3">
              <Button
                primary
                content="Yanıtla"
                onClick={() => {
                  setOpen(true);
                }}
              />
              <Modal open={open} onClose={() => setOpen(false)} className="pv2">
                <Modal.Header>Mesaja cevap ver:</Modal.Header>
                <Modal.Content>
                  <TextArea
                    className="w-100"
                    rows={6}
                    onInput={(event, data) => {
                      setReplyBody(data.value);
                    }}
                    value={replyBody}
                  />
                </Modal.Content>
                <Modal.Actions>
                  <div className="pa2">
                    <Button
                      primary
                      floated="right"
                      content="Gönder"
                      onClick={(event, data) =>
                        sendReply(messageHeaders, replyBody)
                      }
                    />
                  </div>
                </Modal.Actions>
              </Modal>
            </div>
          ) : null}
        </div>
        <div className="pa2 ba br2 b--light-blue vh-25 overflow-y-scroll">
          <p>
            <strong>Message:</strong>
          </p>
          <div>{ReactHtmlParser(messagebody, options)}</div>
        </div>
      </Container>
    </div>
  );
};

export default Message;
