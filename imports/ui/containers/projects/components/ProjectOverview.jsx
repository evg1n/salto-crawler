import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import Emails from "../../../../startup/both/collections/emails.js";

import ProjectItem from "./ProjectItem.jsx";
import GMail from "../components/gmail/GMail.jsx";
import "./projectStyles.css";

import { Icon, Table, Modal, Label } from "semantic-ui-react";


const ProjectOverview = props => {








  const index = props.skip + props.index + 1;

  const backgroundColor =
    props.index % 2 === 1 ? "bg-washed-blue" : "bg-lightest-blue";

  let emails = [];
  let contactPerson = "Yetkili Kişi";

  if (props.project.contactDetails.length > 0) {
    props.project.contactDetails.forEach(item => {
      if (item.startsWith("E-mail:")) {
        emails.push(item.split("E-mail: ")[1]);
      }

      if (item.startsWith("Contact person:")) {
        contactPerson = item.split("Contact person: ")[1];
      }
    });
  }








  return (
    <Table.Row className="black">
      <Table.Cell textAlign="center" width={"one"}>
        {index}
      </Table.Cell>
      <Table.Cell>
        <Modal
          trigger={
            <p>
              {props.project.projectTitle}
              <a href={props.project.projectShortLink}><Icon flipped="horizontally" name="chain"/></a>
            </p>
          }
        >
          <Modal.Content scrolling>
            <ProjectItem data={props.project} />
          </Modal.Content>
        </Modal>
      </Table.Cell>
      <Table.Cell>
        <a href={props.project.organizationLink}>
          {props.project.organizationName}
        </a>
      </Table.Cell>
      <Table.Cell>
        {emails.map((item, index) => (
          <a key={index} href={"mailto:" + item}>
            <p>{item}</p>
          </a>
        ))}
      </Table.Cell>
      <Table.Cell textAlign="center" width="two">
        <Modal
          trigger={
            <Icon.Group className="grow" size="large">
              <Icon name="envelope" />
              <Icon corner color="blue" name="send" />
            </Icon.Group>
          }
        >
          <GMail shortLink={props.project.projectShortLink} projectId={props.project._id}/>


        </Modal>
      </Table.Cell>
      <Table.Cell width="three">
        {typeof props.project.projectRelation === "object"
          ? props.project.projectRelation.map((item, index) => (
              <Label
                key={"label-" + index}
                style={{ marginBottom: "0.5em" }}
                size="tiny"
                icon="tag"
                content={item}
              />
            ))
          : null}
      </Table.Cell>
    </Table.Row>
  );
};

export default ProjectOverview;
