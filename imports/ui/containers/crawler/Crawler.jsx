import React, { useState, useEffect } from "react";
import { saveAs } from "file-saver";
import { Button } from "semantic-ui-react";

import Graph from "./Graph.jsx";
const Crawler = props => {
  const [crawling, toggleCrawling] = useState(false);
  const [green, toggleGreen] = useState(false);
  const [red, toggleRed] = useState(false);

  useEffect(() => {
    props.user ? null : window.location.assign("/login");
  }, [props.user]);

  const getLinks = () => {
    toggleCrawling(true);
    Meteor.call("crawl", (error, result) => {
      if (error) {
        toggleCrawling(false);
        return console.error("Crawling error:", error);
      }
      console.log("result:", result);
      toggleCrawling(false);
    });
  };

  const convertToExcel = async () => {
    toggleCrawling(true);
    await Meteor.call("getExcelBuffer", (error, response) => {
      if (error) {
        return console.error("Excel Error:", error);
      }
      let blob = new Blob([response.buffer], {
        type: "text/vnd.ms-excel;charset=utf-8"
      });
      saveAs(blob, "projects.xlsx");
      toggleCrawling(false);
    });
  };

  const crawlLinks = () => {
    toggleCrawling(true);

    Meteor.call("parseDOM", (error, result) => {
      if (error) {
        toggleCrawling(false);
        return console.error("Crawling error:", error);
      }

      toggleCrawling(false);
    });
  };

  const getContactDetails = () => {
    toggleCrawling(true);
    Meteor.call("getContactDetails", (error, result) => {
      toggleCrawling(false);

      if (error) {
        console.error("Error:", error);
      }

      console.log("result:", result);
    });
  };

  return (
    <div className="w-100 tc ba br2 b--black-20 shadow-5 pa2 mv2">
      <h3>SALTO CRAWLER Kontrol Paneli</h3>
      <p>Mavi butonlara sistem localhost'ta çalışmadığı sürece basmayınız.</p>
      <div className="flex justify-center">
        <div>
          <Button
            primary
            disabled={crawling}
            loading={crawling}
            onClick={getLinks}
          >
            Proje Linklerini Getir
          </Button>
        </div>

        <div>
          <Button
            primary
            disabled={crawling}
            loading={crawling}
            onClick={crawlLinks}
          >
            Projeleri Tara
          </Button>
        </div>

        <div>
          <Button
            primary
            disabled={crawling}
            loading={crawling}
            onClick={getContactDetails}
          >
            Proje İrtibat Bilgilerini Güncelle
          </Button>
        </div>
        <div>
          <Button
            icon="file excel"
            onMouseEnter={() => toggleGreen(true)}
            onMouseLeave={() => toggleGreen(false)}
            content="Excel İndir"
            color="green"
            basic={!green}
            disabled={crawling}
            loading={crawling}
            onClick={convertToExcel}
          />
        </div>
        <div>
          <Button
            icon="arrow left"
            onMouseEnter={() => toggleRed(true)}
            onMouseLeave={() => toggleRed(false)}
            content="Genel Görünüme Dön"
            color="red"
            basic={!red}
            onClick={() => window.location.assign("/")}
          />
        </div>
      </div>
      <div className="flex justify-center mv2 w-100 pa2 ba br2 b--black-20">
        <Graph />
      </div>
    </div>
  );
};

export default Crawler;
