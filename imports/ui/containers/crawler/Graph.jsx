import React, { PureComponent, useEffect } from "react";
import _ from "lodash";

import Emails from "../../../startup/both/collections/emails.js";

import {
  BarChart,
  Bar,
  Cell,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from "recharts";


export default class Graph extends PureComponent {
  constructor() {
    super();
    this.state = {
      data: []
    };
  }

  sentEmails = async () =>
    await Meteor.call("sentEmails", (error, result) => {
      if (error) {
        return console.log("Error getting chart data:", error);
      }

      this.groupByDay(result);
    });

  groupByDay = array => {
    let dailyGroup = _.groupBy(array, item => {
      return item.mailSent.toLocaleDateString("tr-TR", {
        year: "numeric",
        month: "numeric",
        day: "numeric"
      });
    });

    let data = [];

    for (let [key, value] of Object.entries(dailyGroup)) {
      data.push({name: key, value: value.length})
    }

    this.setState({data: data});
  };

  componentDidMount() {
    this.sentEmails();
  }
  render() {
    return (
      <div className="w-100">
        <h3>Günlük E-Posta Gönderim Grafiği</h3>

        <ResponsiveContainer width={"100%"} height={600}>
          <BarChart
            data={this.state.data}
            margin={{
              top: 5,
              right: 30,
              left: 20,
              bottom: 5
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Legend />
            <Bar dataKey="value" fill="#2872EB" />
          </BarChart>
        </ResponsiveContainer>
      </div>
    );
  }
}
