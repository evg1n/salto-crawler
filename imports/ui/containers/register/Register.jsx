import React, { useState } from 'react'
import { Accounts } from "meteor/accounts-base";

//redux
import { connect } from "react-redux";
import {changeEmail, changePassword, changeConfirmPassword} from "../../redux";

import {Form, Button} from "semantic-ui-react";

const Register = (props) => {
  
  const [email] = useState();
  const [password] = useState();
  const [confirmPassword] = useState();
  const [loading, toggleLoading]

  doRegister = (email, password, confirmPassword) => {


    Meteor.call("createUser", {username: username, email:username, password: password},
    
    (error) => {



    }
    )



  }


  return (
    <div className="flex flex-wrap justify-center w-100">
      <h2 className="w-100 tc">Yeni Kullanıcı Kaydı</h2>
      <Form className="flex flex-wrap justify-center pa3 br3 ba b--black-20 w-100 w-50-l w-75-m shadow-5">
        <Form.Field className="w-100">
          <label>Kullanıcı Adı/E-Posta</label>
          <input
            value={email}
            onChange={event => props.changeEmail(event.target.value)}
            type="email"
            autoComplete="email"
          />
        </Form.Field>
        <Form.Field className="w-100">
          <label>Şifre</label>
          <input
            value={password}
            onChange={event => props.changePassword(event.target.value)}
            type="text"
            autoComplete="new-password"
          />
        </Form.Field>
        <Form.Field className="w-100">
          <label>Şifre Tekrarı</label>
          <input
            value={confirmPassword}
            onChange={event => props.changeConfirmPassword(event.target.value)}
            type="text"
            autoComplete="confirm-password"
          />
        </Form.Field>
        <Button className="w-100" type="submit">
          Kayıt Ol
        </Button>
      </Form>
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    register: state.register
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeEmail: (input) => dispatch(changeEmail(input)),
    changePassword: (input) => dispatch(changePassword(input)),
    changeConfirmPassword: (input) => dispatch(changeConfirmPassword(input))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register);
