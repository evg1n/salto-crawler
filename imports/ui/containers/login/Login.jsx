import { Meteor } from "meteor/meteor"
import { Accounts } from "meteor/accounts-base";

import React, { useState, useEffect } from "react";

import Swal from "sweetalert2";

import { Form, Input, Button } from "semantic-ui-react";

const Login = (props) => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);


  if(props.user){
    console.log(props.user)
  }


  const doLogin = () => {
    setLoading(true);
    Meteor.loginWithPassword(username, password, error => {
      setLoading(false);
      if (error) {
        console.error("Login error:", error);
      } else {
        if (Accounts.userId()){
          window.location.assign("/");
        } 
      }
    });
  };

  return (
    <div className={"w-100 tc"}>
      <h3>SALTO CRAWLER</h3>
      <div
        className={
          "flex w-100 ba br2 b--blue min-vh-50 justify-center items-center pv5"
        }
      >
        <div
          className={
            "w-50-l w-75-m w-90-s ba br2 b--light-blue pv3 shadow-5 bg-washed-blue"
          }
        >
          <h4>SALTO CRAWLER Uygulamasına Hoş Geldiniz</h4>
          <p>Lütfen kullanıcı adınız ve parolanız ile giriş yapınız.</p>
          <Form className="pv3">
            <Form.Field className={"ph3 tl"}>
              <label>Kullanıcı Adı:</label>
              <Input
                disabled={loading}
                onChange={(event, data) => setUserName(data.value)}
                placeholder={"kullanici@etkialani.com"}
              />
            </Form.Field>
            <Form.Field className={"ph3 tl"}>
              <label>Parola:</label>
              <Input
                type={"password"}
                disabled={loading}
                onChange={(event, data) => setPassword(data.value)}
                placeholder={"Parola"}
              />
            </Form.Field>
            <Form.Field className={"flex justify-center pa3"}>
              <Button
                icon={"lock"}
                primary
                loading={loading}
                type="submit"
                onClick={doLogin}
                content="Giriş Yap"
              />
              <Button
              disabled
                icon={"signup"}
                color="green"
                onClick={()=> window.location.assign("/register")}
                content="Kayıt Ol"
              />
            </Form.Field>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Login;

/**
 * Meteor.call("login", username, password, (error, response) => {
      if (error) {
        return Swal.fire({
          type: "error",
          title: "Hata!",
          text: "Kullanıcı adı veya parola yanlış."
        });
      } else {
        setLoading(false);
        loginSuccess();
      }
    });
 */
