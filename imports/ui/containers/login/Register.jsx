
import React, { useState, useEffect } from "react";

import Swal from "sweetalert2";

import { Form, Input, Button } from "semantic-ui-react";

const Register = () => {
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [match, setMatch] = useState(true);

  useEffect(()=> {

    if (Accounts.userId()){
      window.location.assign("/")
    }
  }, [loading])




  const doRegister = () => {
    
    setLoading(true);

    if (password !== confirmPassword) {
      setMatch(false);
      Swal.fire({
        icon: "error",
        title: "Hata!",
        text: "Parolalar eşleşmiyor"
      });
    } else if (match) {
      console.log("registering")
      Meteor.call(
        "register",
        { username: username, email: username, password: password , profile: {name: username}},
        error => {
          setLoading(false);
          if (error) {
            console.error("Login error:", error.stack);
            Swal.fire({
              icon: "error",
              title: "Hata!",
              text: "Bir hata oluştu. Ayrıntılar için konsola bakınız."
            });
          } else {
            if (Accounts.userId() === null){
              window.location.assign("/login");
            } else {
              window.location.assign("/");
            }
            
          }
        }
      );
    }
  };

  return (
    <div className={"w-100 tc"}>
      <h3>SALTO CRAWLER</h3>
      <div
        className={
          "flex w-100 ba br2 b--blue min-vh-50 justify-center items-center pv5"
        }
      >
        <div
          className={
            "w-50-l w-75-m w-90-s ba br2 b--light-blue pv3 shadow-5 bg-washed-blue"
          }
        >
          <h4>SALTO CRAWLER Uygulamasına Hoş Geldiniz</h4>
          <p>Lütfen kullanıcı adınız ve parolanız ile giriş yapınız.</p>
          <Form className="pv3">
            <Form.Field className={"ph3 tl"}>
              <label>Kullanıcı Adı:</label>
              <Input
                disabled={loading}
                onChange={(event, data) => setUserName(data.value)}
                placeholder={"kullanici@etkialani.com"}
              />
            </Form.Field>
            <Form.Field className={"ph3 tl"}>
              <label>Parola:</label>
              <Input
                type={"text"}
                disabled={loading}
                onChange={(event, data) => setPassword(data.value)}
                placeholder={"Parola"}
              />
            </Form.Field>
            <Form.Field className={"ph3 tl"}>
              <label>Parola Tekrarı:</label>
              <Input
                type={"text"}
                disabled={loading}
                onChange={(event, data) => setConfirmPassword(data.value)}
                placeholder={"Parola Tekrarı"}
              />
            </Form.Field>
            <Form.Field className={"flex justify-center pa3"}>
              <Button
                icon={"sign-in"}
                primary
                loading={loading}
                type="submit"
                onClick={doRegister}
                content="Giriş Yap"
              />
            </Form.Field>
          </Form>
        </div>
      </div>
    </div>
  );
};

export default Register;

/**
 * Meteor.call("login", username, password, (error, response) => {
      if (error) {
        return Swal.fire({
          type: "error",
          title: "Hata!",
          text: "Kullanıcı adı veya parola yanlış."
        });
      } else {
        setLoading(false);
        loginSuccess();
      }
    });
 */
