//import modules
import { Accounts } from "meteor/accounts-base";
import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import { connect } from "react-redux";

import { Button, Image } from "semantic-ui-react";

//import styles
import "semantic-ui-css/semantic.min.css";
import "./App.css";

//import components
import Projects from "./containers/projects/Projects.jsx";
import Crawler from "./containers/crawler/Crawler.jsx";
import ProjectPagination from "./containers/projects/components/ProjectPagination.jsx";
import Login from "./containers/login/Login.jsx";
import Register from "./containers/login/Register.jsx";
import Search from "./components/search/Search.jsx";

class AppNew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: Accounts.userId(),
      buttonRed: false
    };
  }

  doLogout = () => {
    console.log("Logout");
    Accounts.logout(() => this.setState({ user: null }));
  };

  componentDidMount() {
    if (this.state.user === null && window.location.pathname !== "/login") {
      window.location.assign("/login");
    }
  }

  componentDidUpdate() {
    if (this.state.user === null && window.location.pathname !== "/login") {
      window.location.assign("/login");
    }
  }

  render() {
    return (
      <Router>
        <div className="flex flex-wrap items-start w-100 pa2 vh-100 relative">
          <div className="flex pa2 justify-center w-100">
            <div className="ui small image grow shadow-5">
              <a href="/crawler">
                <img
                  className="br3 b--black-10"
                  id="logo"
                  src="/apple-icon.png"
                />
              </a>
            </div>
          </div>
          {this.state.user !== null ? (
            <div className="pa2 absolute top-0 right-0">
              <Button
                basic={!this.state.buttonRed}
                color="red"
                compact
                onMouseEnter={()=> this.setState({buttonRed: true})}
                onMouseLeave={()=> this.setState({buttonRed: false})}
                icon={"shutdown"}
                floated="right"
                content="Çıkış Yap"
                onClick={this.doLogout}
              />
            </div>
          ) : null}
          <Switch>
            <Route exact path="/">
              <div className="w-100">
                <Search
                  isLoading={this.props.isLoading}
                  totalProjects={this.props.totalProjects}
                />
              </div>
              <div className="w-100">
                <div className="project-container ba br2 b--black-20 overflow-y-scroll shadow-1">
                  <Projects
                    toggleLoading={this.toggleLoading}
                    activePage={this.props.activePage}
                  />
                </div>
                <div className="flex w-100 justify-center pa2">
                  <ProjectPagination />
                </div>
              </div>
            </Route>
            <Route exact path="/login">
              <div className={"flex w-100 justify-center items-center"}>
                <Login user={this.state.user} />
              </div>
            </Route>
            <Route exact path="/crawler">
              <div className="w-100">
                <Crawler user={this.state.user} />
              </div>
            </Route>
            <Route exact path="/register">
              <div className={"flex w-100 justify-center items-center"}>
                <Register />
              </div>
            </Route>
            <Route>
              <div>
                <p>Not found</p>
              </div>
            </Route>
          </Switch>

          <div className="w-100 tc">
            <h6>
              ©2020 Kıyı Doğa Sporları Kulübü Derneği. Tüm hakları saklıdır.
            </h6>
          </div>
        </div>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    activePage: state.pagination.activePage,
    totalPages: state.pagination.totalPages,
    totalProjects: state.pagination.totalProjects,
    search: state.search,
    isLoading: state.loading.isLoading
  };
};

export default connect(mapStateToProps)(AppNew);
