import "../imports/startup/both";
import "../imports/startup/client";

import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import App from '/imports/ui/App'
import AppNew from "/imports/ui/AppNew.jsx";
import {Provider} from "react-redux";

import store from "../imports/ui/redux/store.js";

import "semantic-ui-css/semantic.min.css";
import "tachyons";

Meteor.startup(() => {
  Meteor.subscribe("mainData");
  
  render(
    (
    <Provider store={store}>
      <AppNew/>
      </Provider>
      ), document.getElementById('react-target')
  )
}
)