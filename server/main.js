import { Meteor } from "meteor/meteor";
const schedule = require("node-schedule");
import Emails from "../imports/startup/both/collections/emails.js";

import "../imports/startup/both";
import "../imports/startup/server";
import "../imports/controllers";
import * as GLOBAL_VARS from "../imports/startup/both/lib.js";
import * as CHALK from "../imports/startup/server/chalk.js";

Meteor.startup(() => {
  console.log(
    "╔════════════════════════════════════════════╗\n" +
      `║      ${GLOBAL_VARS.ROBOT}SALTO CRAWLER v1.1 by evg1n        ║\n` +
      "║ Please visit: https://www.github.com/evg1n ║\n" +
      "║  ©2020 kiyidosk.org. All rights reserved.  ║\n" +
      "╚════════════════════════════════════════════╝"
  );

  Meteor.call("updateAllProjects");
});

const automatedCrawling = new schedule.RecurrenceRule();

automatedCrawling.dayOfWeek = [new schedule.Range(0, 6)];
automatedCrawling.minute = Meteor.settings.crawler.autoCrawlingSchedule;

const automatedLinkRefresh = new schedule.RecurrenceRule();

automatedLinkRefresh.dayOfWeek = [new schedule.Range(0, 6)];
automatedLinkRefresh.minute = Meteor.settings.crawler.autoLinkRefresh;

const refreshLinks = schedule.scheduleJob(automatedLinkRefresh, async function () {
  let { success, infoMsg, errorMsg, warnMsg } = CHALK;
  console.log(infoMsg("Auto-refresh links started."));

  Meteor.call("updateAllProjects", (error, result) => {
    if (error) {
      return console.error("Error:", error);
    }

    // chaining functions

    console.log(CHALK.success("Links refreshed."));
    
  });
});

const automatedMailing = new schedule.RecurrenceRule();
automatedMailing.dayOfWeek = [new schedule.Range(0, 6)];
automatedMailing.minute = new schedule.Range(
  0,
  59,
  Meteor.settings.crawler.mailingFrequency
);

const mailingSchedule = schedule.scheduleJob(
  automatedMailing,

  async function () {
    let { success, infoMsg, errorMsg, warnMsg } = CHALK;
    console.log(infoMsg("Auto-mailer started."));

    let contactDetailsArray = Emails.findOne(
      { threadId: { $exists: false }, emails: { $exists: true } },
      { sort: { publishedAt: -1 } }
    );

    Meteor.call("sendBulkMail", [contactDetailsArray], (error, result) => {
      if (error) {
        Emails.update(
          { projectId: contactDetailsArray.projectId },
          { $set: { threadId: "projectFaulty" } }
        );
        return console.error("Mail sending error:", error);
      }

      console.log("Result:", result);
    });
  }
);

const crawlingSchedule = schedule.scheduleJob(
  automatedCrawling,
  async function () {
    console.log(CHALK.infoMsg("Automated crawling started..."));
    Meteor.call("crawl", (error, result) => {
      if (error) {
        return console.error("Error:", error);
      }

      // chaining functions

      console.log(CHALK.success(result.body.message + result.body.linkCount));
      console.log(CHALK.infoMsg("Parsing project details."));

      Meteor.call("parseDOM", (error, result) => {
        if (error) {
          return console.error("Error:", error);
        }

        // chaining functions

        console.log(CHALK.success(result.body.message));
        console.log(CHALK.infoMsg("Parsing project contact details."));

        Meteor.call("getContactDetails", (error, result) => {
          if (error) {
            return console.error("Error:", error);
          }
          // chaining functions

          Meteor.call("updateAllProjects", (error, result) => {
            if (error) {
              return console.error("Error:", error);
            }
            console.log(CHALK.success(result.body.message));
            console.log(CHALK.success("Automated crawling has finished."));
          });
        });
      });
    });
  }
);
